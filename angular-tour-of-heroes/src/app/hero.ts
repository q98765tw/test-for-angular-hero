export interface Hero
  {
    id: number,
    firstName: string,
    lastName: string,
    nickName: string,
    age: number,
    address: {
      street: string,
      city: string,
      state: string,
      zip: string
    },
    aliases: Array<string>
  }
;
