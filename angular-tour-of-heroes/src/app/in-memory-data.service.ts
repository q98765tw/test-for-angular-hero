import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Hero } from './hero';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const heroes = [
      {
        id: 1,
        firstName: 1,
        lastName: 1,
        nickName: "bill",
        age: 1,
        address: {
          street: 1,
          city: 1,
          state: 1,
          zip: 1
        },
        aliases: ['vvvv','wwww']
      },
      {
        id: 2,
        firstName: 2,
        lastName: 2,
        nickName: "bruce",
        age: 2,
        address: {
          street: 2,
          city: 2,
          state: 2,
          zip: 2
        },
        aliases: Array<string>
      },
      {
        id: 3,
        firstName: 3,
        lastName: 3,
        nickName: "nita",
        age: 3,
        address: {
          street: 3,
          city: 3,
          state: 3,
          zip: 3
        },
        aliases: Array<string>
      },
      {
        id: 4,
        firstName: 4,
        lastName: 4,
        nickName: "nick",
        age: 4,
        address: {
          street: 4,
          city: 4,
          state: 4,
          zip: 4
        },
        aliases: Array<string>
      },
      {
        id: 5,
        firstName: 5,
        lastName: 5,
        nickName: "ivy",
        age: 5,
        address: {
          street: 5,
          city: 5,
          state: 5,
          zip: 5
        },
        aliases: Array<string>
      },
      {
        id: 6,
        firstName: 6,
        lastName: 6,
        nickName: "mike",
        age: 6,
        address: {
          street: 6,
          city: 6,
          state: 6,
          zip: 6
        },
        aliases: Array<string>
      },
    ];
    return {heroes};
  }

  // Overrides the genId method to ensure that a hero always has an id.
  // If the heroes array is empty,
  // the method below returns the initial number (11).
  // if the heroes array is not empty, the method below returns the highest
  // hero id + 1.
  // genId(heroes: Hero[]): number {
  //   return heroes.length > 0 ? Math.max(...heroes.map(hero => hero.id)) + 1 : 11;
  // }
}