import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { HeroesComponent } from './heroes/heroes.component';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { AddHeroComponent } from './add-hero/add-hero.component';
import { ReactiveComponent } from "./reactive/reactive.component";
import { AddReactiveComponent } from "./add-reactive/add-reactive.component";

const routes:Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  {path: 'heroes', component: HeroesComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'addhero', component: AddHeroComponent},
  {path: 'addreactive', component: AddReactiveComponent},
  {path: 'detail/:id', component: HeroDetailComponent},
  {path: 'reactive/:id', component: ReactiveComponent},
]
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

