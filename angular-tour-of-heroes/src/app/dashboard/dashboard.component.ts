import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from "../hero.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];
  constructor(
    private heroService:HeroService

  ) { }

  ngOnInit(): void {
    this.getHeroes();
  }

  getHeroes():void{
    this.heroService.getHeroes().subscribe(event =>this.heroes = event)
  }

  addHero(name:string):void{
    //this.heroService.addHero({name} as Hero).subscribe(hero =>this.heroes.push(hero))
  }
  deleteHero(hero:Hero):void{
    this.heroes = this.heroes.filter(e=> e!== hero)
    this.heroService.deleteHero(hero.id).subscribe();
  }
}
