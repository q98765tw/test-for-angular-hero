import { Component, OnInit,Input } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, UntypedFormArray } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormArray } from '@angular/forms';
import { Hero } from '../hero';

import { HeroService } from '../hero.service';
import { Location } from '@angular/common';

import { UniqueAlterEgoValidator } from '../alter-ego.directive';

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.css']
})
export class ReactiveFormComponent implements OnInit {
  @Input() hero!: Hero;
  heroes: Hero[] = [];

  profileForm = this.fb.group({
    id:[0],
    firstName: ['', Validators.required],
    lastName: [''],
    nickName:['', {
      asyncValidators: [this.alterEgoValidator.validate.bind(this.alterEgoValidator)],
      updateOn: 'blur'
    }],
    address: this.fb.group({
      street: [''],
      city: [''],
      state: [''],
      zip: ['']
    }),
    aliases: this.fb.array([
      this.fb.control('')
    ])
  });

  getHeroes():void {
    this.heroService.getHeroes().subscribe(event =>this.heroes = event)
  }
  
  get alterEgo() { return this.profileForm.get('nickName')!; }

  get aliases() {
    return this.profileForm.get('aliases') as FormArray;
  }

  constructor(
    private fb: FormBuilder,
    private heroService:HeroService,
    private location:Location,
    private alterEgoValidator: UniqueAlterEgoValidator,
    ) { }

  ngOnInit(): void {
    this.getHeroes();
    if(this.hero){
      this.profileForm.patchValue({
        firstName: this.hero.firstName,
        lastName: this.hero.lastName,
        nickName:this.hero.nickName,
        address: {
          street: this.hero.address.street,
          city:this.hero.address.city,
          state:this.hero.address.state,
          zip:this.hero.address.zip
        },
      });
      this.hero.aliases.forEach(element => {
        this.aliases.push(new FormControl(
          element
        ))
      });
    }
  }
 
  addAlias() {
    this.aliases.push(this.fb.control(''));
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    if(this.hero){
      this.profileForm.patchValue({id:this.hero.id})
      this.heroService.updateHero(this.profileForm.value as Hero).subscribe();
      this.location.back();
    }else{
      this.profileForm.patchValue({id:null})
      this.heroService.addHero(this.profileForm.value as Hero).subscribe();
      this.location.back();
    }
  }
}
