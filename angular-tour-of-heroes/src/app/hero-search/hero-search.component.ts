import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-hero-search',
  templateUrl: './hero-search.component.html',
  styleUrls: ['./hero-search.component.css']
})
export class HeroSearchComponent implements OnInit {
  heroes$!:Observable<Hero[]>
  constructor(
    private heroService:HeroService,

  ) { }
  
  searchItem = new Subject<string>();
  
  ngOnInit(): void {
    this.heroes$ = this.searchItem.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((keyword:string) => this.heroService.search(keyword))
    )
  }
  searchHero(name:string):void{
    this.searchItem.next(name);
  }
}
