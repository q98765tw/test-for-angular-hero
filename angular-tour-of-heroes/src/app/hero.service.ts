import { Injectable,OnInit } from '@angular/core';
import { Hero } from'./hero';

import { HttpClient,HttpHeaders } from '@angular/common/http';

import { Observable, throwError,of, Subscriber } from 'rxjs';

import { delay, map ,distinctUntilChanged } from 'rxjs/operators';let ALTER_EGOS = ['Eric']
@Injectable({
  providedIn: 'root'
})
export class HeroService implements OnInit{

  private heroesUrl = 'api/heroes';  // URL to web api
  heroes:Hero[] = [];

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  constructor(
    private http: HttpClient,
  ) { }

  ngOnInit():void{
    this.getHeroes().subscribe(e=>this.heroes =e)
    for(let i=0;i<this.heroes.length;i++){
      ALTER_EGOS.push(this.heroes[i].nickName)
    }
  }
  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.heroesUrl);
  }
  getHero(id:number): Observable<Hero>{
    const url = `${this.heroesUrl}/${id}`;
    return this.http.get<Hero>(url);
  }
  addHero(hero:Hero):Observable<Hero>{
    return  this.http.post<Hero>(this.heroesUrl,hero,this.httpOptions)
  }

  updateHero(hero:Hero):Observable<Hero>{
    return  this.http.put<Hero>(this.heroesUrl,hero,this.httpOptions)
  }

  deleteHero(id:Number):Observable<Hero>{
    const url = `${this.heroesUrl}/${id}`;
    return  this.http.delete<Hero>(url, this.httpOptions)
  }

  search(name:string):Observable<Hero[]>{
    if(!name.trim()) return of([]);
    return this.http.get<Hero[]>(`${this.heroesUrl}/?firstName=${name}`)
  }
  isAlterEgoTaken(alterEgo: string): Observable<boolean> {
    // console.log( this.getHeroes())
    // return this.getHeroes().pipe(
    //   map(array => {return array.find(hero => hero.nickName === alterEgo) !== undefined}),
    //   delay(400), 
    // )
    ALTER_EGOS =[];
    this.getHeroes().subscribe(e=>this.heroes =e)
    for(let i=0;i<this.heroes.length;i++){
      ALTER_EGOS.push(this.heroes[i].nickName)
    }
   
    const isTaken = ALTER_EGOS.includes(alterEgo);
    return of(isTaken).pipe(delay(400));
  }
}
