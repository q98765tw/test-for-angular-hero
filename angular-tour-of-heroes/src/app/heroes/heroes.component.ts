import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from "../hero.service";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {
  heroes: Hero[] = [];
  constructor(
    private heroService:HeroService

  ) { }

  ngOnInit(): void {
    this.getHeroes();
  }

  getHeroes():void{
    this.heroService.getHeroes().subscribe(event =>this.heroes = event)
  }

  addHero(name:string):void{
    //this.heroService.addHero({name} as Hero).subscribe(hero =>this.heroes.push(hero))
  }
  deleteHero(hero:Hero):void{
    this.heroes = this.heroes.filter(e=> e!== hero)
    this.heroService.deleteHero(hero.id).subscribe();
  }
}
