import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { HeroService } from './hero.service';
import { Hero } from './hero';

export function forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    
    const forbidden = nameRe.test(control.value);
    return forbidden ? {forbiddenName: {value: control.value}} : null;
  };
}

@Directive({
  selector: '[appForbiddenName]',
  providers: [{provide: NG_VALIDATORS, useExisting: ForbiddenValidatorDirective, multi: true}]
})
export class ForbiddenValidatorDirective implements Validator {
  @Input('appForbiddenName') forbiddenName = '';
  heroes: Hero[] = [];
  constructor(
    private heroService:HeroService,
  ){}
  ngOnInit(): void {
    this.getHeroes();
  }
  getHeroes():void{
    this.heroService.getHeroes().subscribe(event =>this.heroes = event)
  }
  validate(control: AbstractControl): ValidationErrors | null {
    for(let i=0;i<this.heroes.length;i++){
      this.forbiddenName =this.heroes[i].nickName
      const ans=forbiddenNameValidator(new RegExp(this.forbiddenName))(control)
      if(ans!==null) return ans
    }   
    return this.forbiddenName ? forbiddenNameValidator(new RegExp(this.forbiddenName, 'i'))(control)
                              : null;
                            }
}
