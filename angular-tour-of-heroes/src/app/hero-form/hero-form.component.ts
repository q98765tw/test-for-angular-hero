import { Component, OnInit , Input} from '@angular/core';
import { Hero } from '../hero';

import { HeroService } from '../hero.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-hero-form',
  templateUrl: './hero-form.component.html',
  styleUrls: ['./hero-form.component.css']
})
export class HeroFormComponent implements OnInit {
  @Input() hero!: Hero;
  model!: Hero ;
  
  constructor(
    private heroService:HeroService,
    private location:Location
  ) { }

  ngOnInit(): void {
    this.model={
      id:0,
      firstName:'',
      lastName:'',
      age:0,
      nickName:'',
      aliases:[],
      address: {
        street: '1',
        city: '',
        state: '1',
        zip: '1'
    }};
  }
  addAliasHero():void{
    this.hero.aliases=this.hero.aliases||[];
    this.hero.aliases.push("");
  }
  addAliasModel():void{
    this.model.aliases.push("");
  }
  updateHero():void{
    if(this.hero) {
      this.heroService.updateHero(this.hero).subscribe();
    }
    this.location.back();
  }
  addHero():void{
    var mycar = {firstName:this.model.firstName,lastName:this.model.lastName,nickName:this.model.nickName,
      address: {
      street: this.model.address.street,
      city: this.model.address.city,
    },
    aliases:this.model.aliases
    };
    
    this.heroService.addHero(mycar as Hero).subscribe();
    this.location.back();
  }
  goback():void{
    this.location.back();
  }
  trackByIndex(index: number, obj: any): any {
    return index;
  }
}
